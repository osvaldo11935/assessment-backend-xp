<?php
include_once '../Helper/Cors.php';
include_once '../Helper/Help.php';
include_once '../Models/Produtos.php';
include_once '../Models/Categoria.php';
$data=array();
$msg="";
$produtos=new Produtos();
$categorias=new Categorias();
$dataJSon = json_decode(file_get_contents("php://input"), true);
if($_GET["action"]=="add")
{
   
  if ($dataJSon) {
         $categorias->setValorPesquisaTabela($dataJSon["category"]);
         $categorias->setCampoTable("idCategorias");       
         $categoria=$categorias->pesquisar();
         $produtos->setdescricao($dataJSon["description"]);
         $produtos->setquantidade($dataJSon["quantity"]);
         $produtos->setsku($dataJSon["sku"]);
         $produtos->setdesignacao($dataJSon["name"]);
         $produtos->setpreco($dataJSon["price"]);
         $produtos->setcategoriaId($categoria[0]["idCategorias"]);
         $erro=$produtos->validar();
         if(count($erro)==0)
         {
            $msg=$produtos->cadastrar();
            $result= array("mensagem"=>$msg,"data"=>$dataJSon,"status"=>true,"erros"=>array());
         }
         else
         {
            $result= array("mensagem"=>null,"data"=>$dataJSon,"status"=>false,"erros"=>$erro);
         }
  } 
}
if($_GET["action"]=="addForFile")
{
   base64_to_file($dataJSon["data"],"../Arquivo/outPut.txt");
   $dados = file("../Arquivo/outPut.txt");
   unlink("../Arquivo/outPut.txt");
   for($o=1;$o<count($dados);$o++)
   {
      $dat=explode(";",$dados[$o]);
      $categorias->setValorPesquisaTabela($dat[5]);
      $categorias->setCampoTable("designacao");
      $categoria=$categorias->pesquisar();
      if($categoria==false)
      {
            $categorias->setdesignacao($dat[5]);
            $categorias->cadastrar(); 
            $categoria=$categorias->pesquisar();     
      }
      $produtos->setdescricao($dat[2]);
      $produtos->setquantidade($dat[3]);
      $produtos->setsku($dat[1]);
      $produtos->setdesignacao($dat[0]);
      $produtos->setpreco($dat[4]);
      $produtos->setcategoriaId($categoria[0]["idCategorias"]);
      $msg=$produtos->cadastrar();  
   }
   $result= array("mensagem"=>$msg,"status"=>true,"data"=>$dataJSon);
}
if($_GET["action"]=="get")
{
  $res=$produtos->listar();
  $result= array("status"=>true,"data"=>$res);
}
if($_GET["action"]=="remover")
{
   $produtos->setValorPesquisaTabela($_GET["id"]);
   $msg=$produtos->deletar();
   $result= array("mensagem"=>$msg,"status"=>true,"data"=>array());            
}
if($_GET["action"]=="update")
{
  $produtos->setValorPesquisaTabela($_GET["id"]);
  $categorias->setValorPesquisaTabela($dataJSon["category"]);
  $categorias->setCampoTable("idCategorias");       
  $categoria=$categorias->pesquisar();
  $produtos->setdescricao($dataJSon["description"]);
  $produtos->setquantidade($dataJSon["quantity"]);
  $produtos->setsku($dataJSon["sku"]);
  $produtos->setdesignacao($dataJSon["name"]);
  $produtos->setpreco($dataJSon["price"]);
  $produtos->setcategoriaId($categoria[0]["idCategorias"]);
  $msg=$produtos->alterar();
  $result= array("mensagem"=>$msg,"status"=>true,"data"=>$dataJSon);          
}
if($_GET["action"]=="search")
{
     $keys=array_keys($_GET);
     $produtos->setCampoTable($keys[1]);
     $produtos->setValorPesquisaTabela($_GET[$keys[1]]);       
     $res= $produtos->pesquisar();
    $result= array("status"=>true,"data"=>$res);
}
echo  json_encode($result);
