<?php
include_once '../Helper/Cors.php';
include_once '../Models/Categoria.php';
$result=array();
$categoria=new Categorias();
$data = json_decode(file_get_contents("php://input"), true);
if($_GET["action"]=="get")
{
     $res=$categoria->listar();
     $result=array("data"=> $res,"status"=>true);
}
if($_GET["action"]=="search")
{
    $keys=array_keys($_GET);
    $categoria->setCampoTable($keys[1]);
    $categoria->setValorPesquisaTabela($_GET[$keys[1]]);        
    $res= $categoria->pesquisar();
    $result=array("data"=> $res,"status"=>true);
}
if($_GET["action"]=="add")
{
  if ($data) {
      $categoria->setdesignacao($data["category-name"]);
      $categoria->setcodigo($data["category-code"]);
      $msg=$categoria->cadastrar();
      $result=array("data"=> $data,"status"=>true,"mensagem"=>$msg);
  }
}if($_GET["action"]=="remover")
{
   $categoria->setValorPesquisaTabela($_GET["id"]);
   $msg=$categoria->deletar();
   $result=array("data"=> array(),"status"=>true,"mensagem"=>$msg);        
}
if($_GET["action"]=="update")
{
  $categoria->setValorPesquisaTabela($_GET["id"]);
  $categoria->setdesignacao($data["category-name"]);
  $categoria->setcodigo($data["category-code"]);
  $msg=$categoria->alterar();
  $result=array("data"=> $data,"status"=>true,"mensagem"=>$msg);            
}
echo  json_encode($result);
