<?php

use function PHPSTORM_META\type;

include_once '../abstract/Abstrata.php';
class Produtos extends Abstrata {
    private $idProduto;
    private $designacao;
    private $preco;
    private $quantidade;
    private $sku;
    private $descricao;
    private $Categoriasid;

    
    private $campoNaTabela;
    private $valor;
    function getidProduto() {
        return $this->idProduto;
    }
    function getsku() {
        return $this->sku;
    }
    function getquantidade() {
        return $this->quantidade;
    }
    function getdesignacao() {
        return $this->designacao;
    }
    function getpreco() {
        return $this->preco;
    }
    function getdescricao() {
        return $this->descricao;
    }
    function getcategoriaId() {
            return $this->Categoriasid;
        }

    function setidProduto($idProduto) {
        $this->idProduto = $idProduto;
    }
    function setsku($sku) {
        $this->sku = $sku;
    }
    function setquantidade($quantidade) {
        $this->quantidade = $quantidade;
    }

    function setdesignacao($designacao) {
        $this->designacao = $designacao;
    }

    function setpreco($preco) {
        $this->preco = $preco;
    }

    function setdescricao($descricao) {
        $this->descricao = $descricao;
    }
    function setcategoriaId($categoriaId) {
            $this->Categoriasid = $categoriaId;
        }

 //Metodo responsavel por setar o nome do campo a ser pesquisar na tabela
 function setCampoTable($prop) {
    $this->campoNaTabela = $prop;
}
//Metodo responsavel por setar o valor a pesquisar na tabela
function setValorPesquisaTabela($valor) {
    $this->valor = $valor;
}
//Metodo responsavel por alterar os dados do produto
    public function alterar() {
         $pdo = parent::getDB();  
         try {
            $cadastrar = $pdo->prepare("UPDATE  produtos set 
            designacao=:designacao
            ,preco=:preco
            ,descricao=:descricao
            ,Categoriasid=:Categoriasid
            ,quantidade=:quantidade
            ,sku=:sku where idProdutos=:id");
            $cadastrar->bindParam(":designacao", $this->designacao);
            $cadastrar->bindParam(":preco", $this->preco);
            $cadastrar->bindParam(":descricao", $this->descricao);
            $cadastrar->bindParam(":Categoriasid", $this->Categoriasid);
            $cadastrar->bindParam(":quantidade", $this->quantidade);
            $cadastrar->bindParam(":sku", $this->sku);
            $cadastrar->bindParam(":id", $this->valor);
            $cadastrar->execute();
            parent::setmsg("Dados actualizado com sucesso");
            return parent::getmsg();
        } catch (PDOException $e) {
            echo $e->getMessage();
        } 
    }
//Metodo responsavel por cadastrar os dados do produto
    public function cadastrar() {
        $pdo = parent::getDB();
        try {
            $cadastrar = $pdo->prepare("INSERT INTO produtos(designacao,preco,descricao,Categoriasid,quantidade,sku) VALUES(?,?,?,?,?,?)");
            $cadastrar->bindParam(1, $this->designacao);
            $cadastrar->bindParam(2, $this->preco);
            $cadastrar->bindParam(3, $this->descricao);
            $cadastrar->bindParam(4, $this->Categoriasid);
           
            $cadastrar->bindParam(5, $this->quantidade);
            $cadastrar->bindParam(6, $this->sku);
            $cadastrar->execute();
            parent::setmsg("Dados cadastrado com sucesso");
            return parent::getmsg();
        } catch (PDOException $e) {
            echo $e->getMessage();
        } 
            
    }
//Metodo responsavel por deletar os dados do produto
    public function deletar() {
        parent::$tabela = "produtos";
        parent::$propTabela="idProdutos";
        parent::setId($this->valor);
        parent::deletar();
        parent::setmsg("Dados removido com sucesso");
        return parent::getmsg();
    }
//Metodo responsavel por listar os dados do produto    
    public function listar() {
        
        parent::$tabela = "produtos";
        return parent::listar();
    }
    //Metodo responsavel por fazer pesquisa por um campo 
    public function pesquisar() {
        parent::$tabela = "produtos";
        parent::$propTabela=$this->campoNaTabela;
        parent::$search=$this->valor;
        return parent::search();
    }
    public function validar()
    {
      $erros=array();  
    //   $typepreco=gettype($this->preco);
    //   $typequantidade=gettype($this->quantidade);  
    // //   if($typepreco!="double")  
    // //   {
    // //       array_push($erros,"o campo preco o seu valor deve ser do tipo decimal");
    // //   }
    // //   if($typequantidade!="int")  
    // //   {
    // //       array_push($erros,"o campo quantidade o seu  valor deve ser do tipo decimal");
    // //   }
      if(empty($this->preco) || empty($this->quantidade) || empty($this->designacao) || empty($this->Categoriasid) || empty($this->sku))
      {
         array_push($erros,"campo obrigatorio preencher");
      }
      return $erros;
    }
}
?>