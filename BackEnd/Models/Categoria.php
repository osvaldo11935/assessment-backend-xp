<?php
include_once '../abstract/Abstrata.php';
class Categorias extends Abstrata {
    private $idCategorias;
    private $designacao;
    private $codigo;
    private $campoNaTabela;
    private $valor;
    function getidCategoria() {
        return $this->idCategorias;
    }
    function getdesignacao() {
        return $this->designacao;
    }
    function getcodigo() {
        return $this->codigo;
    }
    function setidCategoria($idCategoria) {
        $this->idCategorias = $idCategoria;
    }
    function setcodigo($codigo) {
        $this->codigo = $codigo;
    }
    function setdesignacao($designacao) {
        $this->designacao = $designacao;
    }
    //Metodo responsavel por setar o nome do campo a ser pesquisar na tabela
    function setCampoTable($prop) {
        $this->campoNaTabela = $prop;
    }
    //Metodo responsavel por setar o valor a pesquisar na tabela
    function setValorPesquisaTabela($valor) {
        $this->valor = $valor;
    }
    //Metodo responsavel por alterar os dados da categoria
    public function alterar() {
        $pdo = parent::getDB();  
        try {
           $cadastrar = $pdo->prepare("UPDATE  categorias set 
           designacao=:designacao
           ,codigo=:codigo
            where idCategorias=:id");
           $cadastrar->bindParam(":designacao", $this->designacao);
           $cadastrar->bindParam(":codigo", $this->codigo);
           $cadastrar->bindParam(":id", $this->valor);
           $cadastrar->execute();
           parent::setmsg("Dados actualizado com suceasso");
           return parent::getmsg();
       } catch (PDOException $e) {
           echo $e->getMessage();
       } 
    }
    //Metodo responsavel por cadastrar os dados da categoria
    public function cadastrar() {
        $pdo = parent::getDB();
        try {
            $cadastrar = $pdo->prepare("INSERT INTO categorias(designacao,codigo) VALUES(?,?)");
            $cadastrar->bindParam(1, $this->designacao);
            $cadastrar->bindParam(2, $this->codigo);
            $cadastrar->execute();
            parent::setmsg("Dados cadastrado com suceasso");
            return parent::getmsg();
        } catch (PDOException $e) {
            echo $e->getMessage();
        } 
            
    }
    //Metodo responsavel por deletar os dados da categoria
    public function deletar() {
        parent::$tabela = "categorias";
        parent::$propTabela="idCategorias";
        parent::setId($this->valor);
        parent::deletar();
        parent::setmsg("Dados removido com suceasso");
        return parent::getmsg();
    }
    //Metodo responsavel por listar os dados da categoria
    public function listar() {
        parent::$tabela = "categorias";
        return parent::listar();
    }
     //Metodo responsavel por pesquisar os dados da categoria
    public function pesquisar() {
        parent::$tabela = "categorias";
        parent::$propTabela=$this->campoNaTabela;
        parent::$search=$this->valor;
        return parent::search();
    }

    
}
?>