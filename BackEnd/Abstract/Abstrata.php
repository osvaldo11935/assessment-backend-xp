<?php

include_once '../Dbconnect/connect.php';

abstract class Abstrata extends Connect{
    
    protected static $tabela;
    protected static $propTabela;
    protected static $search;
    private $erro;
    private $msg;
    private $id;
    public function getErro() {
        return $this->erro;
    }
    public function getmsg() {
       return $this->msg;
    }
    public function setmsg($msg) {
        $this->msg=$msg;
    }
    public function setErro($erro) {
        $this->erro = $erro;
    }
    public function getId() {
        return $this->id;
    }
    public function setId($id) {
        $this->id = $id;
    }
    protected function listar(){
                
        $pdo = parent::getDB();
        try {
            $listar = $pdo->prepare("SELECT * FROM ".self::$tabela);
            $listar->execute();

            if ($listar->rowCount() > 0):
                return $listar->fetchAll(PDO::FETCH_ASSOC);
            else:
                return false;
            endif;
        } catch (PDOException $e) {
            echo "Erro: " . $e->getMessage();
        }
    }
    protected function deletar(){
        $pdo = parent::getDB();
        try {
            $deletar = $pdo->prepare("DELETE FROM ". self::$tabela . " WHERE " . self::$propTabela . "= :id");
            $deletar->bindValue(":id", $this->id);
            $deletar->execute();
            
            if($deletar->rowCount() == 1):
                    return true;
                else:
                    return false;;
            endif;
            
        } catch (PDOException $e) {
            echo "Erro: " . $e->getMessage();
        }
      }
      
      protected function existeCadastro(){
          $pdo = parent::getDB();

        try {
            $verifica = $pdo->prepare("SELECT * FROM " . $this->tabela . " WHERE " . $this->propTabela .
                    " = :search");
            $verifica->bindValue(":search", $this->search);
            $verifica->execute();

            if ($verifica->rowCount() > 0):
                return true;
            else:
                return false;
            endif;
        } catch (PDOException $e) {
            echo "Erro: " . $e->getMessage();
        }
                
      }
      protected function search(){
        $pdo = parent::getDB();

      try {
          $a=self::$search;
          $listar = $pdo->prepare("SELECT * FROM " . self::$tabela . " WHERE " . self::$propTabela .
                  " = :search");
          $listar->bindValue(":search", self::$search);
          $listar->execute();
          
          if($listar->rowCount() > 0):
            return $listar->fetchAll(PDO::FETCH_ASSOC);
          else:
              return false;
          endif;
      } catch (PDOException $e) {
          echo "Erro: " . $e->getMessage();
      }
              
    }
}

?>
