const toBase64 = file => new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.readAsDataURL(file);
            reader.onload = () => resolve(reader.result);
            reader.onerror = error => reject(error);
        });
        
        async function Main(id) {
           const file = document.querySelector(`#${id}`).files[0];
         return await toBase64(file);
        }
        
     