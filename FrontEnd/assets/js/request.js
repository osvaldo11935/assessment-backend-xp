

const ENDPOINT = 'http://localhost/assessment-backend-xp/BackEnd'
const requestConfig=async(method,obj)=>{
    let requeConfig={body: JSON.stringify(obj),headers: { 'Content-Type': 'application/json' },method: method}
    if(method=="GET") delete requeConfig.body
    return  requeConfig;
}
const requestAPI = async(url,method, obj = {}) => {
    const response = await fetch(ENDPOINT+url,await requestConfig(method,obj));
    return response.json();
}
const addForCSV =async(id,url,id_form=null)=>{
  let obj={}
  obj["data"]= await Main(id);
  let inputs=document.querySelectorAll(`.${id_form} input[type=text], .${id_form} select, .${id_form} textarea`)
  for(let i=0;i<inputs.length;i++)
  {
    inputs[i].disabled=true
  }
  let result=await requestAPI(url,"POST",obj)
  document.getElementById("msg").textContent=result.mensagem;
  alert(result.mensagem);
  window.location.reload()
}
const addForForm =async(id_form,url)=>{
   let obj={}
   let inputs=document.querySelectorAll(`.${id_form} input[type=text], .${id_form} select, .${id_form} textarea, .${id_form} file`)
   for(let i=0;i<inputs.length;i++)
   {
       obj[inputs[i].id]=inputs[i].value
   }
   let result=await requestAPI(url,"POST",obj)
   document.getElementById("msg").textContent=result.mensagem;
   alert(result.mensagem);
   window.location.reload()
}
const addProduto =async(id_form,url)=>{
    let arrayErro=[]
    let inputs=document.querySelectorAll(`.${id_form} input[type=text], .${id_form} select, .${id_form} textarea, .${id_form} file`)
    for(let i=0;i<inputs.length;i++)
    {
        if(inputs[i].value=="")
        {
            document.getElementById(inputs[i].id+"erro").textContent="Campo obrigatorio"
            arrayErro.push("Campo obrigatorio")
        }
        if(inputs[i].id=="price")
        {
           let isFlat= parseFloat(inputs[i].value)
           if(isFlat.toString()=="NaN")
           {
              document.getElementById(inputs[i].id+"erro").textContent="o valor passado neste campo deve ser decimal obrigatorio"

              arrayErro.push("o valor passado neste campo deve ser decimal obrigatorio")

           }
          
            
        }
        if(inputs[i].id=="quantity")
        {
            let isInt= parseInt(inputs[i].value)
            if(isInt.toString()=="NaN")
            {
              document.getElementById(inputs[i].id+"erro").textContent="o valor passado neste campo deve ser inteiro obrigatorio"
              arrayErro.push("o valor passado neste campo deve ser inteiro obrigatorio")
            }
              
         }
        
    }
    if(arrayErro.length==0)
     addForForm(id_form,url)
 }

const setDataSelect=async(id,url)=>{
    html="";
    let result=await requestAPI(url,"GET")
    result.data.map((elemet)=>{
        let option=document.createElement("option")
        option.setAttribute("value",elemet.idCategorias)
        option.append(document.createTextNode(elemet.designacao))
        document.getElementById(id).append(option)
    })
}
const setDataTable =async(id,url,urlJoin=null,form=null)=>{
    let th=document.querySelectorAll(`#${id} th`)
    let result=await requestAPI(url,"GET")
    for(let r=0;r<result.data.length;r++){
        result.data[r]["editar"]="editar"
        result.data[r]["remover"]="remover"
        let tr=document.createElement("tr")
        tr.setAttribute("class","data-row")
        for(let a =0;a<th.length;a++)
        {
            if(th[a].id!="" && th[a].id!="action")
            {
                let td=document.createElement("td")
                td.setAttribute("class","data-grid-td")
                let span=document.createElement("span")
                span.setAttribute("class","data-grid-cell-content")
                let lastString= th[a].id[th[a].id.length-2]+th[a].id[th[a].id.length-1]
                if(lastString=="id")
                {
                  let res= await  requestAPI(`${urlJoin}${result.data[r][th[a].id]}`,"GET")
                  span.append(document.createTextNode(res.data[0].designacao))
                }
                else
                 span.append(document.createTextNode(result.data[r][th[a].id]))
                td.append(span)
                tr.append(td)
            }
            if(th[a].id=="action")
            {
               var idvalue= Object.keys(result.data[r])
                let td=document.createElement("td")
                td.setAttribute("class","data-grid-td")
                let a=document.createElement("a")
                a.setAttribute("href","#")
                a.setAttribute("onclick",`getDadosForActualizar('http://localhost/assessment-backend-xp/FrontEnd/assets/${form}.html','${url.replace("get","search")}&${idvalue[0]}=${result.data[r]["id"+id]}')`)
                a.append(document.createTextNode(result.data[r]["editar"]))
                let a1=document.createElement("a")
                a1.setAttribute("href","#")
                a1.setAttribute("onclick",`deletar('${url.replace("get","remover")}&id=${result.data[r]["id"+id]}')`)
                a1.append(document.createTextNode(result.data[r]["remover"]))
                td.append(a)
                let br=document.createElement("br")
                td.append(br)
                td.append(a1)
                tr.append(td)
            }
        }
         document.getElementsByTagName("tbody")[0].append(tr)
    }
}
const actualizar=async(id_form,url,id)=>{
    let obj={}
    let inputs=document.querySelectorAll(`.${id_form} input[type=text], .${id_form} select, .${id_form} textarea, .${id_form} file`)
    
    for(let i=0;i<inputs.length;i++)
    {
        obj[inputs[i].id]=inputs[i].value
    }
    let result=await requestAPI(url,"POST",obj)
    document.getElementById("msg").textContent=result.mensagem;
    alert(result.mensagem);
    window.location.reload()
}
const isActualizar=async(id,url,table)=>{
    let datalocal=localStorage.getItem("actualizar")
    let inputs=document.querySelectorAll(`.${id} input[type=submit],.${id} input[type=text],  .${id} textarea`)
    
    if(datalocal.length>0)
    {
        let data=JSON.parse(datalocal);
        for (let l = 0; l < inputs.length; l++) {
            if(inputs[l].type=="submit")
            inputs[l].type="hidden"
            else
            inputs[l].value=data[inputs[l].name]
        }
        let input=document.createElement("input")
        input.setAttribute("type","submit")
        input.setAttribute("value","Actualizar")
        input.setAttribute("class","btn-submit btn-action")
        input.setAttribute("onclick",`actualizar('${id}','${url}${data["id"+table]}')`)
        document.getElementsByClassName("actions-form")[0].append(input)
        localStorage.removeItem("actualizar")
    }
    
 }
const getDadosForActualizar=async(urlred,url)=>{
   let res= await  requestAPI(`${url}`,"GET")
   localStorage.setItem("actualizar",JSON.stringify(res.data[0]))
   window.location.href=urlred;
   //"http://localhost/assessment-backend-xp/FrontEnd/assets/addCategory.html"
}
const deletar=async(url)=>{
    let result=await requestAPI(url,"GET")
    if(result)
    {
        alert("Dados deletado com sucesso");
        window.location.reload()
    }
   
}